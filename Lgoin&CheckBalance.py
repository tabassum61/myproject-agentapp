from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from appium.webdriver.common.mobileby import MobileBy
# noinspection PyUnresolvedReferences
#from .mobilecommand import MobileCommand as Command

from time import sleep

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '6.0.1'
desired_caps['deviceName'] = '37a400eb'
desired_caps["app"] = '/Users/kazi.ferdous/Desktop/AgentApp.apk'
desired_caps['appPackage'] = 'com.bkash.businessapp.sit'
#desired_caps['automationName'] = 'UiAutomator2'
desired_caps['noReset'] = 'true'
desired_caps['autoGrantPermissions'] = 'true'
#desired_caps['appActivity']= 'com.bkash.businessapp.ui.activity.SplashActivity'

# Adding appWait Activity since the activity name changes as the focus shifts to the ATP WTA app's first page
driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
sleep(10)


#Log_IN
#Suru_Korun
#sleep(2)
#driver.find_element_by_id('com.android.packageinstaller:id/permission_allow_button').click()
#sleep(2)
#driver.find_element_by_id('com.android.packageinstaller:id/permission_allow_button').click()
#sleep(2)
#driver.find_element_by_id('com.android.packageinstaller:id/permission_allow_button').click()


button = driver.find_element_by_xpath("//android.widget.RelativeLayout[@index='3']")
button.click()

#Number_Input
input = driver.find_element_by_xpath("//android.widget.EditText[@text='এজেন্ট নাম্বার দিন']")
input.send_keys('01743015086')
button = driver.find_element_by_xpath("//android.widget.ImageView[@index='2']")
button.click()

sleep(20)

#PIN_Number
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_1']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_2']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_1']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_2']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_1']").click()


button = driver.find_element_by_xpath("//android.widget.ImageView[@resource-id='com.bkash.businessapp.sit:id/iv_next_arrow']")
button.click()


sleep(20)
#Condition_Accept
driver.find_element_by_xpath("//android.widget.ImageView[@resource-id='com.bkash.businessapp.sit:id/next_layout_arrow']").click()

#bKash_HomePage(BalanceCheck)
sleep(5)
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/tv_balance']").click()
sleep(2)
balance = driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/tv_show_balance']")
value = balance.get_attribute("text")
print (value)
sleep(5)

#Log_Out

driver.find_element_by_xpath("//android.widget.TextView[@content-desc='বিকাশ এজেন্ট']").click()
driver.find_element_by_xpath("//android.widget.Button[@resource-id='com.bkash.businessapp.sit:id/btn_logout']").click()
driver.find_element_by_xpath("//android.widget.Button[@text='হ্যাঁ']").click()

sleep(5)
driver.close_app()

driver.launch_app()
sleep(10)
#PIN_Number
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_1']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_2']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_1']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_2']").click()
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/pinpad_button_1']").click()
button = driver.find_element_by_xpath("//android.widget.ImageView[@resource-id='com.bkash.businessapp.sit:id/iv_next_arrow']")
button.click()

sleep(5)
driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/tv_balance']").click()
sleep(2)
balance1 = driver.find_element_by_xpath("//android.widget.TextView[@resource-id='com.bkash.businessapp.sit:id/tv_show_balance']")
value1 = balance1.get_attribute("text")
print(value1)
if value == value1:
    print("Pass")
else:
    print("Fail")

sleep(5)

driver.quit()

